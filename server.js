const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API TechU"});
  }
);
/*
app.get('/apitechu/v1/users',
  function(req,res){
    console.log("GET /apitechu/v1/users");

    //res.sendFile('usuarios.json',{root: __dirname});
    var users = require('./usuarios.json');
    res.send(users);
  }
);
*/
//////////////////////////// Práctica1 inicio
app.get('/apitechu/v1/users/',
  function(req,res){
    console.log("GET /apitechu/v1/users/");

    //res.sendFile('usuarios.json',{root: __dirname});
    var users = require('./usuarios.json');

    if (req.query.$top != undefined){
      var usersTop = new Array();
      var index=0;
      for (user of users) {
           console.log("Length of array is " + users.length);
           console.log("top is " + req.query.$top);
           console.log("count is " + req.query.$count);
           if (user != null && index < req.query.$top) {
             console.log("El top coincide");
             usersTop[index]= user;
           }else{
              if (req.query.$count == 'true'){
                var count = {"Count" : users.length};
                usersTop[index]= count;
              }
              break;
           }

           index += 1;
         }
       }else{
         var usersTop= require('./usuarios.json');
         console.log("No hay queryParam " + usersTop.length)
       }

    res.send(usersTop);
  }
);
//////////////////////////// Práctica1 fin
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
    //console.log(req.headers);
    console.log("First_name is: " + req.body.first_name);
    console.log("Last_name is: " + req.body.last_name);
    console.log("Email is: " + req.body.email);


    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email is" : req.body.email
    };

    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

    console.log("Usuario añadido con éxito");

    res.send({"msg":"Usuario añadido con éxito"});
  }
);
//////////////////////////// Práctica2_ Login y Logout - inicio
app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");
    //console.log(req.headers);
    console.log("Email is: " + req.body.email);
    console.log("Password is: " + req.body.password);

    var users = require('./usuarios.json');

    var userId = '';
    for (user of users) {
         console.log("User is " + user.first_name);
         console.log("email is " + user.email);
         console.log("password is " + user.password);

         if (user.email == req.body.email && user.password == req.body.password) {
           console.log("El email y password coinciden");
           user.logged = true;
           userId = user.id;
           writeUserDataToFile(users);
           break;
         }
         //index += 1;
      }

    if (userId != ''){
      var logInUser = {
        "mensaje" : "Login correcto",
        "idUsuario" : userId
      };

    }else{
      var logInUser = {
        "mensaje" : "Login incorrecto"
      };
    }



    console.log("Usuario logado con éxito");

    res.send(logInUser);
  }
);

app.post('/apitechu/v1/logout',
  function(req,res){
    console.log("POST /apitechu/v1/login");
    //console.log(req.headers);
    console.log("Email is: " + req.body.email);
    console.log("Password is: " + req.body.password);

    var users = require('./usuarios.json');

    var userId = '';
    for (user of users) {
        console.log("User is " + user.first_name);
        console.log("email is " + user.email);
        console.log("password is " + user.password);

        if (user.email == req.body.email && user.password == req.body.password) {
            console.log("El email y password coinciden");
            delete user.logged;;
            userId = user.id;
            writeUserDataToFile(users);
            break;
        }
    }

         if (userId != ''){
           var logInUser = {
             "mensaje" : "Logout correcto",
             "idUsuario" : userId
           };
           console.log("Usuario deslogado con éxito");
         }else{
           var logInUser = {
             "mensaje" : "Logout incorrecto"
           };
           console.log("Usuario deslogado sin éxito");
         }


    res.send(logInUser);
  }
);
//////////////////////////// Práctica2_ Login y Logout - fin

app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("id es " + req.params.id);

    var users = require('./usuarios.json');
    //users.splice(req.params.id - 1,1); //le quita los datos que se le indica por parametro y devuelve la lista reducida

    var index=0;
    let iterable = users;
    for (let user of iterable){
      console.log(iterable[index].id);
      if (iterable[index].id == req.params.id){
        console.log("El usuario borrado es el " + iterable[index].first_name);
        console.log("Ha llegado a la iteración " + index);

        //delete users[user.id-1];
        users.splice(index,1);
        break;
      }
      value += 1;
    }

    writeUserDataToFile(users);

    console.log("Usuario borrado");
  }
);

function writeUserDataToFile(data){
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json",jsonUserData,"utf8",
    function(err){
      if (err){
        console.log(err);
      }else{
        console.log("Datos escritos en fichero");
      }
    }
  )
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
)
